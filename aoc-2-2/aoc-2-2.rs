#![allow(non_snake_case)]
use std::fs::File;
use std::io::prelude::*;

fn main() {
    let mut f = File::open("aoc-2-2-input.txt").expect("file not found");

    let mut contents = String::new();
    f.read_to_string(&mut contents)
        .expect("something went wrong reading the file");

    let strings: &[&str] = &contents.split("\n").collect::<Vec<&str>>()[..];
    for s1 in strings {
        for s2 in strings {
            let d = differ(s1,s2);
            if d.0 == 1 {
                println!("{}", d.1);
            }
        }
    }
}

fn differ(s1: &str, s2: &str) -> (i32, String) {
    let mut diffCount: i32 = 0;
    let mut sameString = String::new();
    for (c1, c2) in s1.chars().zip(s2.chars()) {
        if c1 == c2 {
            sameString.push(c1);
        }
        else {
            diffCount += 1;
        }
    }
    return (diffCount, sameString);
}