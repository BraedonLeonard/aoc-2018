#![allow(non_snake_case)]
use std::fs::File;
use std::io::prelude::*;

fn main() {
    let mut f = File::open("aoc-2-1-input.txt").expect("file not found");

    let mut contents = String::new();
    f.read_to_string(&mut contents)
        .expect("something went wrong reading the file");

    let strings: Vec<String> = contents.split("\n").map(|s| s.to_string()).collect();
    let mut twoCount = 0;
    let mut threeCount = 0;
    for s in strings {
        let mut two = false;
        let mut three = false;
        for c in s.chars() {
            let cCount = s.chars().filter(|q| *q == c).count();
            if cCount == 2 {
                two = true;
            }
            else if cCount == 3 {
                three = true;
            }
        }
        twoCount += if two {1} else {0};
        threeCount += if three {1} else {0};
    }
    println!("{}", twoCount*threeCount);
}