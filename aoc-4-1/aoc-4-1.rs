#![allow(non_snake_case)]
use std::fs::File;
use std::io::prelude::*;
use std::cmp::Ordering;
use std::collections::HashMap;

#[derive(PartialEq)]
#[derive(Eq)]
enum EventType {
    ShiftStart,
    FallAsleep,
    WakeUp
}

#[derive(Eq)]
struct Date(usize, usize, usize, usize, usize);
#[derive(Eq)]
struct Event(Date, EventType, i16);

impl Ord for Date {
    fn cmp(&self, other: &Date) -> Ordering {
        let yCmp = self.0.cmp(&other.0);
        match yCmp { Ordering::Equal => {}, _ => return yCmp, }
        let moCmp = self.1.cmp(&other.1);
        match moCmp { Ordering::Equal => {}, _ => return moCmp, }
        let dCmp = self.2.cmp(&other.2);
        match dCmp { Ordering::Equal => {}, _ => return dCmp, }
        let hCmp = self.3.cmp(&other.3);
        match hCmp { Ordering::Equal => {}, _ => return hCmp, }
        let miCmp = self.4.cmp(&other.4);
        return miCmp;
    }
}

impl PartialOrd for Date {
    fn partial_cmp(&self, other: &Date) -> Option<Ordering> {
        return Some(self.cmp(other));
    }
}

impl PartialEq for Date {
    fn eq(&self, other: &Date) -> bool {
        return self.cmp(other) == Ordering::Equal;
    }
}

impl Ord for Event {
    fn cmp(&self, other: &Event) -> Ordering {
        return self.0.cmp(&other.0);
    }
}

impl PartialOrd for Event {
    fn partial_cmp(&self, other: &Event) -> Option<Ordering> {
        return Some(self.cmp(other));
    }
}

impl PartialEq for Event {
    fn eq(&self, other: &Event) -> bool {
        return self.cmp(other) == Ordering::Equal;
    }
}

fn main() {
    let mut f = File::open("input.txt").expect("file not found");

    let mut contents = String::new();
    f.read_to_string(&mut contents)
        .expect("something went wrong reading the file");

    let mut events: Vec<Event> = contents.split("\n").map(|x| stringToEvent(x)).collect();
    events.sort();

    let mut currentId: i16 = -1;
    let mut sleepMin: usize = 0;
    let mut guardSleepTimes: HashMap<i16, [usize; 60]> = HashMap::new();
    for e in &events {
        match e.1 {
            EventType::ShiftStart => {
                currentId = e.2;
            },
            EventType::FallAsleep => {
                sleepMin = (e.0).4
            },
            EventType::WakeUp => {
                let currentSleepTimes = guardSleepTimes.entry(currentId).or_insert([0; 60]);
                for minute in sleepMin..(e.0).4 {
                    currentSleepTimes[minute] += 1;
                }
            },
        }
    }

    let mut currentMax = 0;
    let mut currentMaxId = -1;
    for sleepTime in &guardSleepTimes {
        let totalSleepTime = sleepTime.1.iter().fold(0, |acc, x| acc + x);
        if totalSleepTime > currentMax {
            currentMax = totalSleepTime;
            currentMaxId = *sleepTime.0;
        }
    }
    match guardSleepTimes.get(&currentMaxId) {
        Some(&sleepTimes) => {
            let m = sleepTimes.iter().enumerate().max_by_key(|x| x.1).unwrap();
            println!("Minute: {}", m.0);
            println!("ID: {}", currentMaxId);
        },
        _ => panic!("MAAAAPPP"),
    }
}
// Turns 1518-10-05 00:10 to (1518, 10, 05, 00, 10)
fn stringToDate(dateString: &str) -> Date {
    let s1: Vec<&str> = dateString.split("-").collect();
    let s2: Vec<&str> = s1[2].split(" ").collect();
    let s3: Vec<&str> = s2[1].split(":").collect();
    return Date(
        s1[0].parse().unwrap(), 
        s1[1].parse().unwrap(),
        s2[0].parse().unwrap(),
        s3[0].parse().unwrap(),
        s3[1].parse().unwrap()
    );
}

// [1518-10-05 00:10] falls asleep
fn stringToEvent(eventString: &str) -> Event {
    let q: Vec<&str> = eventString.trim_matches('[').split("] ").collect();
    let et;
    let mut id : i16 = -1;
    match &*q[1] {
        "falls asleep" => et = EventType::FallAsleep,
        "wakes up" => et = EventType::WakeUp,
        _ => {
            et = EventType::ShiftStart;
            id = q[1].split(" ").collect::<Vec<&str>>()[1].trim_matches('#').parse().unwrap();
        },
    }
    return Event(stringToDate(q[0]), et, id);
}