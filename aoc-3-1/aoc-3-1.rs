#![allow(non_snake_case)]
use std::fs::File;
use std::io::prelude::*;
use std::collections::HashMap;

fn main() {
    let mut f = File::open("input.txt").expect("file not found");

    let mut contents = String::new();
    f.read_to_string(&mut contents)
        .expect("something went wrong reading the file");

    let lines: &[&str] = &contents.split("\n").collect::<Vec<&str>>()[..];
    let mut numTiles: HashMap<(i32, i32), i32> = HashMap::new();
    // let res = lineToRect("#1 @ 483,830: 24x18");
    // println!("{}, {}, {}, {}", res.0, res.1, res.2, res.3);
    for l in lines {
        let rect = lineToRect(l);
        for x in rect.0..(rect.0 + rect.2) {
            for y in rect.1..(rect.1 + rect.3) {
                let count = numTiles.entry((x,y)).or_insert(0);
                *count += 1;
            }
        }
    }
    let mut totalOverlap: i32 = 0;
    for (_key, val) in numTiles.iter() {
        if *val > 1 {
            totalOverlap += 1;
        }
    }
    println!("{}", totalOverlap);
}

// Returns (left, top, width, height)
fn lineToRect(line: &str) -> (i32, i32, i32, i32) {
    let s: &[&str] = &line.split(" ").collect::<Vec<&str>>()[..];
    let lt = &s[2].trim_matches(':').split(",").map(|p| p.parse::<i32>().unwrap())
        .collect::<Vec<i32>>()[..];
    let wh = &s[3].split("x").map(|p| p.parse::<i32>().unwrap())
        .collect::<Vec<i32>>()[..];
    return (lt[0], lt[1], wh[0], wh[1]);
}